# Back-end Programming Test

This is Loan CRUD Mini-Application by Chatchai Siwilai

## Installation

1. Clone this project
2. Install Vender
```bash
composer install
```
3. Copy and edit .env.example
```bash
cp .env.example .env
```
4. Generate APP_KEY
```bash
php artisan key:generate
```
5. Import Database to your Database file in folder DB 
