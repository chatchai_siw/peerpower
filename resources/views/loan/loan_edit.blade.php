@extends('layouts.app')

@section('content')
    <h2>Create Loan</h2>
    <form method="POST" action="/loan/{{Arr::get($loan, 'id')}}">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label text-right">Loan Amount:</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input type="text" class="form-control" name="amount" value="{{number_format(Arr::get($loan, 'amount'), 0, '.', '')}}">
                    <div class="input-group-append">
                        <span class="input-group-text">฿</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label text-right">Loan Term:</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input type="text" class="form-control" name="term" value="{{Arr::get($loan, 'term')}}">
                    <div class="input-group-append">
                        <span class="input-group-text">Years</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label text-right">Interest Rate:</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input type="text" class="form-control" name="interest_rate" value="{{Arr::get($loan, 'interest_rate')}}">
                    <div class="input-group-append">
                        <span class="input-group-text">%</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label text-right">Start Date:</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <div class="col">
                        <select class="form-control" name="start_month">
                            <?php
                                $months = [
                                    '1' => 'January',
                                    '2' => 'February',
                                    '3' => 'March',
                                    '4' => 'April',
                                    '5' => 'May',
                                    '6' => 'June',
                                    '7' => 'July',
                                    '8' => 'August',
                                    '9' => 'September',
                                    '10' => 'October',
                                    '11' => 'November',
                                    '12' => 'December'
                                ];
                            ?>
                            @foreach($months as $key => $month)
                                <option value="{{$key}}" @if(Arr::get($loan, 'start_month') == $key) selected @endif>{{$month}}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="col">
                        <select class="form-control" name="start_year">
                            @for($i = 2017; $i <= 2050; $i++)
                                <option value="{{$i}}" @if(Arr::get($loan, 'start_year') == $i) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label text-right"></label>
            <div class="col-sm-10">
                <input type="submit" class="btn btn-primary">
                <a href="/loan" class="btn btn-secondary">Back</a>
            </div>
        </div>

    </form>
@endsection
