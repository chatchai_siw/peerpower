@extends('layouts.app')

@section('content')
    <h2>Loan Details</h2>
    <div class="row">
       <div class="col-2">
           ID:
       </div>
       <div class="col">
           {{Arr::get($loan, 'id')}}
       </div>
    </div>

    <div class="row">
        <div class="col-2">
            Loan Amount:
        </div>
        <div class="col">
            {{number_format(Arr::get($loan, 'amount'), 2)}} ฿
        </div>
    </div>

    <div class="row">
        <div class="col-2">
            Loan Term:
        </div>
        <div class="col">
            {{Arr::get($loan, 'term')}} Years
        </div>
    </div>

    <div class="row">
        <div class="col-2">
            Interest Rate:
        </div>
        <div class="col">
            {{Arr::get($loan, 'interest_rate')}} %
        </div>
    </div>

    <div class="row">
        <div class="col-2">
            Created At:
        </div>
        <div class="col">
            {{Arr::get($loan, 'created_at')}}
        </div>
    </div>
    <br>
    <a href="/loan" type="button" class="btn btn-light">Back</a>

    <h2>Repayment Schedules</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Payment No</th>
                <th scope="col">Date</th>
                <th scope="col">Payment Amount</th>
                <th scope="col">Principal</th>
                <th scope="col">Interest</th>
                <th scope="col">Balance</th>
            </tr>
        </thead>
        <tbody>
            @foreach(Arr::get($loan, 'payment') as $key => $item_payment)
                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td> {{\Carbon\Carbon::create(Arr::get($item_payment, 'year'), Arr::get($item_payment, 'month'))->format('M Y') }}</td>
                    <td>{{number_format(Arr::get($item_payment, 'amount'), 2)}}</td>
                    <td>{{number_format(Arr::get($item_payment, 'principal'), 2)}}</td>
                    <td>{{number_format(Arr::get($item_payment, 'interest'), 2)}}</td>
                    <td>{{number_format(Arr::get($item_payment, 'balance'), 2)}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/loan" type="button" class="btn btn-light">Back</a>
@endsection
