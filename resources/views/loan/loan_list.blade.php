@extends('layouts.app')

@section('content')
    <h3>All Loans</h3>
    <a href="/loan/create" class="btn btn-primary">Add new loan</a>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Loan Amount</th>
                <th scope="col">Loan Term</th>
                <th scope="col">Interest Rate</th>
                <th scope="col">Created at</th>
                <th scope="col">Edit</th>
            </tr>
        </thead>
        <tbody>
            @foreach($loans as $loan)
                <tr>
                    <th scope="row">{{Arr::get($loan, 'id')}}</th>
                    <td>{{number_format(Arr::get($loan, 'amount'), 2)}} ฿</td>
                    <td>{{Arr::get($loan, 'term')}} Years</td>
                    <td>{{Arr::get($loan, 'interest_rate')}} %</td>
                    <td>{{Arr::get($loan, 'created_at', '-')}}</td>
                    <td>

                        <form method="POST" action="/loan/{{Arr::get($loan, 'id')}}">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <a href="/loan/{{Arr::get($loan, 'id')}}" class="btn btn-primary btn-sm">View</a>
                            <a href="/loan/{{Arr::get($loan, 'id')}}/edit"  class="btn btn-success btn-sm">Edit</a>
                            <input type="submit"  class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @if($loans)
        {{ $loans->links() }}
    @endif
@endsection
