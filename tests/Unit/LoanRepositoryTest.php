<?php

namespace Tests\Unit;

use App\Repositories\LoanRepository;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class LoanRepositoryTest extends TestCase
{

    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetSuccess()
    {
        $input = [
            'id' => 1,
            'amount' => "10000.000000",
            'term' => 1,
            'interest_rate' => 0.1,
            'start_month' => 1,
            'start_year' => 2017,
            'created_at' => "2020-01-18 17:26:20",
            'updated_at' => "2020-01-18 19:00:52",
            'deleted_at' => "2020-01-18 19:00:52"
        ];
        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('with->find->toArray')->andReturn($input);

        $loan_repository = new LoanRepository();
        $get = $loan_repository->get(1);

        $expect = [
            'status_code' => 200,
            'status_txt' => 'success',
            'data' => $input
        ];

        $this->assertEquals($get,$expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetFail()
    {
        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('with->find->toArray')->andThrow(new \Error('some error', 400));

        $loan_repository = new LoanRepository();
        $get = $loan_repository->get(1);

        $expect = [
            'status_code' => 400,
            'status_txt' => 'some error',
            'data' => null
        ];

        $this->assertEquals($get,$expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDeleteSuccess()
    {
        $DB_mock = \Mockery::mock('overload:'. DB::class);
        $DB_mock->shouldReceive('beginTransaction')->andReturn(true);
        $DB_mock->shouldReceive('commit')->andReturn(true);
        $DB_mock->shouldReceive('rollBack')->andReturn(true);

        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('find->delete')->andReturn(true);

        $payment_mock = \Mockery::mock('overload:App\Models\LoanPayment');
        $payment_mock->shouldReceive('where->delete')->andReturn(true);

        $loan_repository = new LoanRepository();
        $delete = $loan_repository->delete(1);

        $expect = [
            'status_code' => 200,
            'status_txt' => 'success',
            'data' => null
        ];

        $this->assertEquals($delete, $expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDeleteFail()
    {
        $DB_mock = \Mockery::mock('overload:'. DB::class);
        $DB_mock->shouldReceive('beginTransaction')->andReturn(true);
        $DB_mock->shouldReceive('commit')->andReturn(true);
        $DB_mock->shouldReceive('rollBack')->andReturn(true);

        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('find->delete')->andReturn(false);

        $payment_mock = \Mockery::mock('overload:App\Models\LoanPayment');
        $payment_mock->shouldReceive('where->delete')->andThrow(new \Error('some error', 400));

        $loan_repository = new LoanRepository();
        $delete = $loan_repository->delete(1);

        $expect = [
            'status_code' => 400,
            'status_txt' => 'some error',
            'data' => null
        ];

        $this->assertEquals($delete, $expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testPaginationSuccess()
    {
        $input = '{"current_page":1,"data":[{"id":32,"amount":"10000.000000","term":1,"interest_rate":0.15,"start_month":1,"start_year":2017,"created_at":"2020-01-19 11:46:35","updated_at":"2020-01-19 11:46:35","deleted_at":null},{"id":31,"amount":"10000.000000","term":1,"interest_rate":1.05,"start_month":1,"start_year":2017,"created_at":"2020-01-19 11:46:27","updated_at":"2020-01-19 11:46:27","deleted_at":null},{"id":30,"amount":"10000.000000","term":1,"interest_rate":1,"start_month":1,"start_year":2017,"created_at":"2020-01-19 11:46:07","updated_at":"2020-01-19 11:46:07","deleted_at":null},{"id":29,"amount":"1000.000000","term":1,"interest_rate":1,"start_month":1,"start_year":2017,"created_at":"2020-01-19 11:40:22","updated_at":"2020-01-19 11:40:22","deleted_at":null},{"id":21,"amount":"10000.000000","term":1,"interest_rate":1,"start_month":8,"start_year":2034,"created_at":"2020-01-19 09:49:47","updated_at":"2020-01-19 09:49:47","deleted_at":null},{"id":20,"amount":"10000.000000","term":1,"interest_rate":1,"start_month":8,"start_year":2034,"created_at":"2020-01-19 09:48:00","updated_at":"2020-01-19 09:48:00","deleted_at":null},{"id":19,"amount":"10000.000000","term":1,"interest_rate":1,"start_month":8,"start_year":2034,"created_at":"2020-01-19 09:38:47","updated_at":"2020-01-19 09:38:47","deleted_at":null},{"id":18,"amount":"1000.000000","term":1,"interest_rate":0.01,"start_month":1,"start_year":2017,"created_at":"2020-01-19 08:59:57","updated_at":"2020-01-19 08:59:57","deleted_at":null},{"id":17,"amount":"10000.000000","term":1,"interest_rate":0.1,"start_month":5,"start_year":2017,"created_at":"2020-01-19 08:48:22","updated_at":"2020-01-19 08:59:42","deleted_at":null},{"id":16,"amount":"1000.000000","term":1,"interest_rate":1,"start_month":3,"start_year":2017,"created_at":"2020-01-19 08:42:59","updated_at":"2020-01-19 08:42:59","deleted_at":null},{"id":15,"amount":"10000.000000","term":1,"interest_rate":0.1,"start_month":1,"start_year":2017,"created_at":"2020-01-18 19:37:12","updated_at":"2020-01-18 19:37:12","deleted_at":null},{"id":14,"amount":"1000000.000000","term":2,"interest_rate":3,"start_month":1,"start_year":2017,"created_at":"2020-01-18 19:35:48","updated_at":"2020-01-18 19:35:48","deleted_at":null},{"id":12,"amount":"10000.000000","term":2,"interest_rate":0.1,"start_month":4,"start_year":2017,"created_at":"2020-01-18 18:04:48","updated_at":"2020-01-18 18:04:48","deleted_at":null},{"id":11,"amount":"10000.000000","term":2,"interest_rate":0.1,"start_month":4,"start_year":2017,"created_at":"2020-01-18 18:04:10","updated_at":"2020-01-18 18:04:10","deleted_at":null},{"id":10,"amount":"10000.000000","term":1,"interest_rate":0.1,"start_month":6,"start_year":2038,"created_at":"2020-01-18 18:03:26","updated_at":"2020-01-18 18:38:07","deleted_at":null}],"first_page_url":"http:\/\/localhost:8000\/loan?page=1","from":1,"last_page":2,"last_page_url":"http:\/\/localhost:8000\/loan?page=2","next_page_url":"http:\/\/localhost:8000\/loan?page=2","path":"http:\/\/localhost:8000\/loan","per_page":15,"prev_page_url":null,"to":15,"total":22}';
        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('orderBy->paginate')->andReturn(json_decode($input, 1));

        $loan_repository = new LoanRepository();
        $pagination = $loan_repository->pagination();

        $expect = [
            'status_code' => 200,
            'status_txt' => 'success',
            'data' => json_decode($input, 1)
        ];
        $this->assertEquals($pagination, $expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testPaginationFail()
    {
        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('orderBy->paginate')->andThrow(new \Error('some error', 400));

        $loan_repository = new LoanRepository();
        $pagination = $loan_repository->pagination();

        $expect = [
            'status_code' => 400,
            'status_txt' => 'some error',
            'data' => null
        ];
        $this->assertEquals($pagination, $expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateSuccess()
    {
        $DB_mock = \Mockery::mock('overload:'. DB::class);
        $DB_mock->shouldReceive('beginTransaction')->andReturn(true);
        $DB_mock->shouldReceive('commit')->andReturn(true);
        $DB_mock->shouldReceive('rollBack')->andReturn(true);

        $loan = json_decode('{"amount":"10000","term":"1","interest_rate":"1","start_month":"1","start_year":"2022","updated_at":"2020-01-19 14:45:33","created_at":"2020-01-19 14:45:33","id":40}', 1);
        $payments = json_decode('[{"month":1,"year":2022,"amount":1349.957698828313,"principal":516.6243654949797,"interest":833.3333333333333,"balance":9483.37563450502,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1693},{"month":2,"year":2022,"amount":1349.957698828313,"principal":559.6763959528946,"interest":790.2813028754183,"balance":8923.699238552126,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1694},{"month":3,"year":2022,"amount":1349.957698828313,"principal":606.3160956156357,"interest":743.6416032126772,"balance":8317.383142936491,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1695},{"month":4,"year":2022,"amount":1349.957698828313,"principal":656.8424369169387,"interest":693.1152619113742,"balance":7660.540706019552,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1696},{"month":5,"year":2022,"amount":1349.957698828313,"principal":711.579306660017,"interest":638.3783921682959,"balance":6948.961399359535,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1697},{"month":6,"year":2022,"amount":1349.957698828313,"principal":770.8775822150184,"interest":579.0801166132945,"balance":6178.083817144517,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1698},{"month":7,"year":2022,"amount":1349.957698828313,"principal":835.1173807329366,"interest":514.8403180953763,"balance":5342.96643641158,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1699},{"month":8,"year":2022,"amount":1349.957698828313,"principal":904.7104957940146,"interest":445.2472030342983,"balance":4438.255940617565,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1700},{"month":9,"year":2022,"amount":1349.957698828313,"principal":980.1030371101825,"interest":369.8546617181304,"balance":3458.1529035073827,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1701},{"month":10,"year":2022,"amount":1349.957698828313,"principal":1061.7782902026977,"interest":288.1794086256152,"balance":2396.374613304685,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1702},{"month":11,"year":2022,"amount":1349.957698828313,"principal":1150.2598143862558,"interest":199.6978844420571,"balance":1246.1147989184294,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1703},{"month":12,"year":2022,"amount":1349.957698828313,"principal":1246.1147989184437,"interest":103.84289990986912,"balance":-1.432454155292362e-11,"loan_id":40,"updated_at":"2020-01-19 14:45:57","created_at":"2020-01-19 14:45:57","id":1704}]', 1);
        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('create')->andReturn($loan);
        $loan_mock->shouldReceive('find->payment->createMany')->andReturn($payments);

        $loan_repository = new LoanRepository();
        $input = json_decode('{"amount":"10000","term":"1","interest_rate":"1","start_month":"1","start_year":"2022"}', 1);
        $create = $loan_repository->create($input);

        $expect = [
            'status_code' => 200,
            'status_txt' => 'success',
            'data' => [
                'loan' => $loan,
                'payments' => $payments
            ]
        ];
        $this->assertEquals($create, $expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateFail()
    {
        $DB_mock = \Mockery::mock('overload:'. DB::class);
        $DB_mock->shouldReceive('beginTransaction')->andReturn(true);
        $DB_mock->shouldReceive('commit')->andReturn(true);
        $DB_mock->shouldReceive('rollBack')->andReturn(true);

        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('create')->andThrow(new \Error('some error', 400));

        $loan_repository = new LoanRepository();
        $input = json_decode('{"amount":"10000","term":"1","interest_rate":"1","start_month":"1","start_year":"2022"}', 1);
        $create = $loan_repository->create($input);

        $expect = [
            'status_code' => 400,
            'status_txt' => 'some error',
            'data' => null
        ];
        $this->assertEquals($create, $expect);
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateSuccess()
    {
        $DB_mock = \Mockery::mock('overload:'. DB::class);
        $DB_mock->shouldReceive('beginTransaction')->andReturn(true);
        $DB_mock->shouldReceive('commit')->andReturn(true);
        $DB_mock->shouldReceive('rollBack')->andReturn(true);

        $loan_mock = \Mockery::mock('overload:App\Models\Loan');
        $loan_mock->shouldReceive('find->update->payment->createMany')->andReturn(true);
        $loan_mock->shouldReceive('find->payment->createMany')->andReturn(true);

        $payments_mock = \Mockery::mock('overload:App\Models\LoanPayment');
        $payments_mock->shouldReceive('where->delete')->andReturn(true);

        $loan_repository = new LoanRepository();
        $input = json_decode('{"amount":"10000","term":"1","interest_rate":"1","start_month":"1","start_year":"2022"}', 1);
        $update = $loan_repository->update(1, $input);

        $expect = [
            'status_code' => 200,
            'status_txt' => 'success',
            'data' => null
        ];
        $this->assertEquals($update, $expect);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateFail()
    {
        $DB_mock = \Mockery::mock('overload:'. DB::class);
        $DB_mock->shouldReceive('beginTransaction')->andReturn(true);
        $DB_mock->shouldReceive('commit')->andReturn(true);
        $DB_mock->shouldReceive('rollBack')->andReturn(true);

        $payments_mock = \Mockery::mock('overload:App\Models\LoanPayment');
        $payments_mock->shouldReceive('where->delete')->andThrow(new \Error('some error', 400));

        $loan_repository = new LoanRepository();
        $input = json_decode('{"amount":"10000","term":"1","interest_rate":"1","start_month":"1","start_year":"2022"}', 1);
        $update = $loan_repository->update(1, $input);

        $expect = [
            'status_code' => 400,
            'status_txt' => 'some error',
            'data' => null
        ];
        $this->assertEquals($update, $expect);
    }

    public function testCalculatePayment1()
    {
        $input = [
            'amount' => '10000',
            'term' => '1',
            'interest_rate' => '0.1',
            'start_month' => '1',
            'start_year' => '2017',
        ];

        $loan_repository = new LoanRepository();
        $calculate = $loan_repository->calculatePayment($input);

        $expect = json_decode('[{"month":1,"year":2017,"amount":879.1588723000987,"principal":795.8255389667653,"interest":83.33333333333333,"balance":9204.174461033235},{"month":2,"year":2017,"amount":879.1588723000987,"principal":802.4574184581551,"interest":76.70145384194363,"balance":8401.717042575081},{"month":3,"year":2017,"amount":879.1588723000987,"principal":809.144563611973,"interest":70.01430868812568,"balance":7592.572478963108},{"month":4,"year":2017,"amount":879.1588723000987,"principal":815.8874349754061,"interest":63.27143732469256,"balance":6776.685043987702},{"month":5,"year":2017,"amount":879.1588723000987,"principal":822.6864969335345,"interest":56.47237536656418,"balance":5953.998547054167},{"month":6,"year":2017,"amount":879.1588723000987,"principal":829.542217741314,"interest":49.616654558784724,"balance":5124.456329312853},{"month":7,"year":2017,"amount":879.1588723000987,"principal":836.4550695558249,"interest":42.70380274427377,"balance":4288.0012597570285},{"month":8,"year":2017,"amount":879.1588723000987,"principal":843.42552846879,"interest":35.733343831308574,"balance":3444.5757312882383},{"month":9,"year":2017,"amount":879.1588723000987,"principal":850.4540745393633,"interest":28.704797760735318,"balance":2594.121656748875},{"month":10,"year":2017,"amount":879.1588723000987,"principal":857.5411918271914,"interest":21.617680472907292,"balance":1736.5804649216836},{"month":11,"year":2017,"amount":879.1588723000987,"principal":864.6873684257513,"interest":14.471503874347363,"balance":871.8930964959324},{"month":12,"year":2017,"amount":879.1588723000987,"principal":871.8930964959659,"interest":7.26577580413277,"balance":-3.353761712787673e-11}]', 1);

        $this->assertEquals($calculate, $expect);

    }

    public function testCalculatePayment2()
    {
        $input = [
            'amount' => '1000000',
            'term' => '5',
            'interest_rate' => '0.5',
            'start_month' => '6',
            'start_year' => '2020',
        ];

        $loan_repository = new LoanRepository();
        $calculate = $loan_repository->calculatePayment($input);

        $expect = json_decode('[{"month":6,"year":2020,"amount":45604.74166055046,"principal":3938.074993883798,"interest":41666.666666666664,"balance":996061.9250061162},{"month":7,"year":2020,"amount":45604.74166055046,"principal":4102.161451962289,"interest":41502.58020858817,"balance":991959.7635541538},{"month":8,"year":2020,"amount":45604.74166055046,"principal":4273.084845794052,"interest":41331.65681475641,"balance":987686.6787083598},{"month":9,"year":2020,"amount":45604.74166055046,"principal":4451.130047702136,"interest":41153.611612848326,"balance":983235.5486606577},{"month":10,"year":2020,"amount":45604.74166055046,"principal":4636.593799689726,"interest":40968.147860860736,"balance":978598.954860968},{"month":11,"year":2020,"amount":45604.74166055046,"principal":4829.785208010129,"interest":40774.95645254033,"balance":973769.1696529579},{"month":12,"year":2020,"amount":45604.74166055046,"principal":5031.026258343882,"interest":40573.71540220658,"balance":968738.1433946141},{"month":1,"year":2021,"amount":45604.74166055046,"principal":5240.652352441546,"interest":40364.089308108916,"balance":963497.4910421725},{"month":2,"year":2021,"amount":45604.74166055046,"principal":5459.012867126607,"interest":40145.728793423856,"balance":958038.4781750459},{"month":3,"year":2021,"amount":45604.74166055046,"principal":5686.471736590218,"interest":39918.269923960244,"balance":952352.0064384557},{"month":4,"year":2021,"amount":45604.74166055046,"principal":5923.4080589481455,"interest":39681.33360160232,"balance":946428.5983795075},{"month":5,"year":2021,"amount":45604.74166055046,"principal":6170.216728070984,"interest":39434.52493247948,"balance":940258.3816514366},{"month":6,"year":2021,"amount":45604.74166055046,"principal":6427.309091740608,"interest":39177.432568809854,"balance":933831.0725596959},{"month":7,"year":2021,"amount":45604.74166055046,"principal":6695.113637229799,"interest":38909.62802332066,"balance":927135.9589224661},{"month":8,"year":2021,"amount":45604.74166055046,"principal":6974.076705447711,"interest":38630.66495510275,"balance":920161.8822170184},{"month":9,"year":2021,"amount":45604.74166055046,"principal":7264.663234841362,"interest":38340.0784257091,"balance":912897.2189821771},{"month":10,"year":2021,"amount":45604.74166055046,"principal":7567.357536293086,"interest":38037.384124257376,"balance":905329.861445884},{"month":11,"year":2021,"amount":45604.74166055046,"principal":7882.664100305301,"interest":37722.07756024516,"balance":897447.1973455787},{"month":12,"year":2021,"amount":45604.74166055046,"principal":8211.108437818017,"interest":37393.633222732446,"balance":889236.0889077607},{"month":1,"year":2022,"amount":45604.74166055046,"principal":8553.237956060439,"interest":37051.503704490024,"balance":880682.8509517002},{"month":2,"year":2022,"amount":45604.74166055046,"principal":8909.622870896288,"interest":36695.118789654174,"balance":871773.228080804},{"month":3,"year":2022,"amount":45604.74166055046,"principal":9280.85715718363,"interest":36323.88450336683,"balance":862492.3709236204},{"month":4,"year":2022,"amount":45604.74166055046,"principal":9667.559538732945,"interest":35937.18212181752,"balance":852824.8113848874},{"month":5,"year":2022,"amount":45604.74166055046,"principal":10070.37451951349,"interest":35534.36714103697,"balance":842754.436865374},{"month":6,"year":2022,"amount":45604.74166055046,"principal":10489.973457826549,"interest":35114.76820272391,"balance":832264.4634075474},{"month":7,"year":2022,"amount":45604.74166055046,"principal":10927.055685235988,"interest":34677.685975314474,"balance":821337.4077223114},{"month":8,"year":2022,"amount":45604.74166055046,"principal":11382.349672120821,"interest":34222.39198842964,"balance":809955.0580501906},{"month":9,"year":2022,"amount":45604.74166055046,"principal":11856.614241792522,"interest":33748.12741875794,"balance":798098.4438083981},{"month":10,"year":2022,"amount":45604.74166055046,"principal":12350.639835200542,"interest":33254.10182534992,"balance":785747.8039731976},{"month":11,"year":2022,"amount":45604.74166055046,"principal":12865.249828333897,"interest":32739.491832216565,"balance":772882.5541448637},{"month":12,"year":2022,"amount":45604.74166055046,"principal":13401.301904514476,"interest":32203.439756035987,"balance":759481.2522403493},{"month":1,"year":2023,"amount":45604.74166055046,"principal":13959.689483869242,"interest":31645.05217668122,"balance":745521.5627564801},{"month":2,"year":2023,"amount":45604.74166055046,"principal":14541.343212363794,"interest":31063.39844818667,"balance":730980.2195441163},{"month":3,"year":2023,"amount":45604.74166055046,"principal":15147.232512878953,"interest":30457.50914767151,"balance":715832.9870312373},{"month":4,"year":2023,"amount":45604.74166055046,"principal":15778.367200915574,"interest":29826.374459634888,"balance":700054.6198303218},{"month":5,"year":2023,"amount":45604.74166055046,"principal":16435.79916762039,"interest":29168.942492930073,"balance":683618.8206627014},{"month":6,"year":2023,"amount":45604.74166055046,"principal":17120.624132937905,"interest":28484.117527612558,"balance":666498.1965297635},{"month":7,"year":2023,"amount":45604.74166055046,"principal":17833.983471810316,"interest":27770.758188740147,"balance":648664.2130579532},{"month":8,"year":2023,"amount":45604.74166055046,"principal":18577.066116469083,"interest":27027.67554408138,"balance":630087.1469414841},{"month":9,"year":2023,"amount":45604.74166055046,"principal":19351.11053798863,"interest":26253.631122561834,"balance":610736.0364034955},{"month":10,"year":2023,"amount":45604.74166055046,"principal":20157.40681040482,"interest":25447.334850145642,"balance":590578.6295930906},{"month":11,"year":2023,"amount":45604.74166055046,"principal":20997.298760838356,"interest":24607.442899712107,"balance":569581.3308322523},{"month":12,"year":2023,"amount":45604.74166055046,"principal":21872.18620920662,"interest":23732.555451343844,"balance":547709.1446230457},{"month":1,"year":2024,"amount":45604.74166055046,"principal":22783.527301256894,"interest":22821.214359293568,"balance":524925.6173217888},{"month":2,"year":2024,"amount":45604.74166055046,"principal":23732.840938809262,"interest":21871.9007217412,"balance":501192.77638297953},{"month":3,"year":2024,"amount":45604.74166055046,"principal":24721.70931125965,"interest":20883.032349290814,"balance":476471.0670717199},{"month":4,"year":2024,"amount":45604.74166055046,"principal":25751.780532562134,"interest":19852.96112798833,"balance":450719.2865391578},{"month":5,"year":2024,"amount":45604.74166055046,"principal":26824.771388085555,"interest":18779.970272464907,"balance":423894.51515107224},{"month":6,"year":2024,"amount":45604.74166055046,"principal":27942.470195922455,"interest":17662.271464628007,"balance":395952.04495514976},{"month":7,"year":2024,"amount":45604.74166055046,"principal":29106.739787419225,"interest":16498.001873131238,"balance":366845.30516773055},{"month":8,"year":2024,"amount":45604.74166055046,"principal":30319.520611895023,"interest":15285.22104865544,"balance":336525.78455583553},{"month":9,"year":2024,"amount":45604.74166055046,"principal":31582.833970723983,"interest":14021.90768982648,"balance":304942.95058511157},{"month":10,"year":2024,"amount":45604.74166055046,"principal":32898.78538617081,"interest":12705.956274379649,"balance":272044.1651989408},{"month":11,"year":2024,"amount":45604.74166055046,"principal":34269.5681105946,"interest":11335.173549955865,"balance":237774.5970883462},{"month":12,"year":2024,"amount":45604.74166055046,"principal":35697.46678186937,"interest":9907.274878681092,"balance":202077.13030647684},{"month":1,"year":2025,"amount":45604.74166055046,"principal":37184.86123111393,"interest":8419.880429436535,"balance":164892.2690753629},{"month":2,"year":2025,"amount":45604.74166055046,"principal":38734.230449077004,"interest":6870.511211473455,"balance":126158.03862628591},{"month":3,"year":2025,"amount":45604.74166055046,"principal":40348.15671778855,"interest":5256.584942761912,"balance":85809.88190849737},{"month":4,"year":2025,"amount":45604.74166055046,"principal":42029.32991436307,"interest":3575.4117461873902,"balance":43780.55199413429},{"month":5,"year":2025,"amount":45604.74166055046,"principal":43780.5519941282,"interest":1824.1896664222622,"balance":6.097252480685711e-9}]', 1);

        $this->assertEquals($calculate, $expect);

    }
}
