<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanPayment extends Model
{
    use SoftDeletes;

    protected $table = 'loan_payment';
    protected $fillable = [
        'loan_id', 'month', 'year', 'amount', 'principal', 'interest', 'balance', 'created_at'
    ];

    public function loan()
    {
        return $this->hasOne('App\Models\Loan', 'id', 'loan_id');
    }

}
