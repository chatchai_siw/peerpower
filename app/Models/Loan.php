<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use SoftDeletes;

    protected $table = 'loan';
    protected $fillable = [
        'amount', 'term', 'interest_rate', 'start_month', 'start_year', 'created_at'
    ];

    public function payment()
    {
        return $this->hasMany('App\Models\LoanPayment', 'loan_id', 'id');
    }

}
