<?php

namespace App\Repositories;

use App\Models\Loan;
use App\Models\LoanPayment;
use App\Repositories\Interfaces\LoanRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class LoanRepository implements LoanRepositoryInterface
{
    private $perpage = 15;

    public function pagination()
    {
        try {
            $loans = Loan::orderBy('id', 'desc')->paginate($this->perpage);

            $result = [
                'status_code' => 200,
                'status_txt' => 'success',
                'data' => $loans
            ];
        } catch (\Error $exception) {
            $result = [
                'status_code' => 400,
                'status_txt' => $exception->getMessage(),
                'data' => null
            ];
        }

        return $result;
    }

    public function create($input)
    {
        try {
            $payment_array = $this->calculatePayment($input);

            DB::beginTransaction();
            $loan = Loan::create($input);
            $payments = Loan::find(Arr::get($loan, 'id'))->payment()->createMany($payment_array);
            DB::commit();

            $result = [
                'status_code' => 200,
                'status_txt' => 'success',
                'data' => [
                    'loan' => $loan,
                    'payments' => $payments
                ]
            ];
        } catch (\Error $exception) {
            DB::rollBack();

            $result = [
                'status_code' => 400,
                'status_txt' => $exception->getMessage(),
                'data' => null
            ];
        }

        return $result;
    }

    public function get($id)
    {
        try {
            $loan = Loan::with('payment')->find($id)->toArray();

            $result = [
                'status_code' => 200,
                'status_txt' => 'success',
                'data' => $loan
            ];
        } catch (\Error $exception) {
            $result = [
                'status_code' => 400,
                'status_txt' => $exception->getMessage(),
                'data' => null
            ];
        }

        return $result;
    }

    public function update($id, $input)
    {
        try {
            $payment_array = $this->calculatePayment($input);

            DB::beginTransaction();
            LoanPayment::where('loan_id', $id)->delete();

            $loan = Loan::find($id);
            $loan->update($input);

            $payment = Loan::find($id);
            $payment->payment()->createMany($payment_array);
            DB::commit();

            $result = [
                'status_code' => 200,
                'status_txt' => 'success',
                'data' => null
            ];
        } catch (\Error $exception) {
            DB::rollBack();

            $result = [
                'status_code' => 400,
                'status_txt' => $exception->getMessage(),
                'data' => null
            ];
        }

        return $result;
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();

            Loan::find($id)->delete();
            LoanPayment::where('loan_id', $id)->delete();

            DB::commit();

            $result = [
                'status_code' => 200,
                'status_txt' => 'success',
                'data' => null
            ];
        } catch (\Error $exception) {
            DB::rollBack();

            $result = [
                'status_code' => 400,
                'status_txt' => $exception->getMessage(),
                'data' => null
            ];
        }

        return $result;
    }

    public function calculatePayment($input)
    {
        $amount = (float)Arr::get($input, 'amount');
        $interest_rate = (float)Arr::get($input, 'interest_rate');
        $term = (float)Arr::get($input, 'term');

        $PMT = ($amount * ($interest_rate / 12)) / (1 - pow((1 + ($interest_rate / 12)), (-12 * $term)));

        $date = Carbon::create(Arr::get($input, 'start_year'), Arr::get($input, 'start_month'));

        $payment_array = [];
        for ($i = 0; $i < $term * 12; $i++) {
            $interest = ($interest_rate / 12) * $amount;
            $principle = $PMT - $interest;
            $amount = $amount - $principle;
            $payment_array[] = [
                'month' => $date->month,
                'year' => $date->year,
                'amount' => $PMT,
                'principal' => $principle,
                'interest' => $interest,
                'balance' => $amount
            ];
            $date->addMonth();
        }

        return $payment_array;
    }
}
