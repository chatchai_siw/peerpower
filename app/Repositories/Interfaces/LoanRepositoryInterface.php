<?php

namespace App\Repositories\Interfaces;

interface LoanRepositoryInterface
{
    public function pagination();

    public function create($input);

    public function get($id);

    public function update($id, $input);

    public function delete($id);
}
