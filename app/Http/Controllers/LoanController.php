<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoanRequest;
use App\Models\Loan;
use App\Models\LoanPayment;
use App\Repositories\Interfaces\LoanRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LoanController extends Controller
{

    private $loan_repository;

    public function __construct(LoanRepositoryInterface $loan_repository)
    {
        $this->middleware('auth');
        $this->loan_repository = $loan_repository;
    }

    public function index()
    {
        $loans = $this->loan_repository->pagination();

        $view = view('loan.loan_list', [
            'loans' => Arr::get($loans, 'data')
        ]);

        if (Arr::get($loans, 'status_code') != 200) {
            $view->withErrors([
                'message' => Arr::get($loans, 'status_txt'),
            ]);
        }

        return $view;
    }

    public function create()
    {
        return view('loan.loan_create');
    }

    public function store(LoanRequest $request)
    {
        $input = $request->all();
        $loan = $this->loan_repository->create($input);

        if (Arr::get($loan, 'status_code') == 200) {
            $redirect = redirect('loan/'. Arr::get($loan, 'data.loan')->id)->with('status', 'This loan has been created successfully.');
        } else {
            $redirect = redirect()->back()->withErrors(Arr::get($loan, 'status_txt'));
        }

        return $redirect;
    }

    public function show($id)
    {
        $loan = $this->loan_repository->get($id);

        $view = view('loan.loan_detail', [
            'loan' => Arr::get($loan, 'data')
        ]);

        //TODO create error page
        if (Arr::get($loan, 'status_code') != 200) {
            $view->withErrors(Arr::get($loan, 'status_txt'));
        }

        return $view;
    }

    public function edit($id)
    {
        $loan = $this->loan_repository->get($id);

        $view = view('loan.loan_edit', [
            'loan' => Arr::get($loan, 'data')
        ]);

        //TODO create error page
        if (Arr::get($loan, 'status_code') != 200) {
            $view->withErrors(Arr::get($loan, 'status_txt'));
        }

        return $view;
    }

    public function update(LoanRequest $request, $id)
    {
        $input = $request->all();
        $loan = $this->loan_repository->update($id, $input);

        if (Arr::get($loan, 'status_code') == 200) {
            $redirect = redirect('loan/'. $id)->with('status', 'This loan has been updated successfully.');
        } else {
            $redirect = redirect()->back()->withErrors([
                'message' => Arr::get($loan, 'status_txt'),
            ]);
        }

        return $redirect;
    }

    public function destroy($id)
    {
        $delete = $this->loan_repository->delete($id);

        if (Arr::get($delete, 'status_code') == 200) {
            $redirect = redirect('loan/')->with('status', "The loan #$id has been deleted successfully.");
        } else {
            $redirect = redirect()->back()->withErrors([
                'message' => Arr::get($delete, 'status_txt'),
            ]);
        }

        return $redirect;
    }

}
