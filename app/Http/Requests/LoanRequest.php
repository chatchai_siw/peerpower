<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|integer|between:1000,100000000',
            'term' => 'required|integer|between:1,50',
            'interest_rate' => 'required|regex:/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/|between:0,36',
            'start_month' => 'required|integer|between:1,12',
            'start_year' => 'required|integer|between:2017,2050',
        ];
    }
}
