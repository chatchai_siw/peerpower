<?php

namespace App\Providers;

use App\Repositories\Interfaces\LoanRepositoryInterface;
use App\Repositories\LoanRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(LoanRepositoryInterface::class, LoanRepository::class);
    }

    public function boot()
    {
        //
    }
}
